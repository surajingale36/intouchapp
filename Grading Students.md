import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    static int[] solve(int[] grades)
    {
        int n=grades[0];
        int temp=0;
        
        for(int i=0; i<grades.length; i++)
        {
            temp=grades[i];
            if((temp % 5) == 3)
            {
                temp=temp+2;
                grades[i]=temp;
            }
            else if((temp % 5) == 4)
            {
                temp=temp+1;
                grades[i]=temp;
            }
            else 
            {
                grades[i]=temp;
            }
            grades[i]=temp;
        }   
       /* for(int j=0; j<grades.length-1; j++)
        {
            grades[j]=grades[j+1];
        }*/
        return grades;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] grades = new int[n];
        for(int grades_i=0; grades_i < n; grades_i++){
            grades[grades_i] = in.nextInt();
        }
        int[] result = solve(grades);
        for (int i = 0; i < result.length; i++) {
            System.out.println(result[i]);//+ (i != result.length - 1 ? "\n" : ""));
        }
        System.out.print("");
        

    }
}
